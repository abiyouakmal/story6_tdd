from django.contrib import admin
from .models import statusmessage;


class EventAdmin(admin.ModelAdmin):
    pass


admin.site.register(statusmessage, EventAdmin)
