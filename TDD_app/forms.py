from django import forms
from .models import statusmessage
from django.forms import ModelForm


class statusForm(forms.ModelForm):
    class Meta:
        model = statusmessage
        fields = ['statusField']

        widgets = {
            'statusField': forms.TextInput(
                attrs={'class': 'form-control', 'type': 'text', 'placeholder': 'Isi status anda'}),
        }
