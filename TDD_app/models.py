from django.db import models


class statusmessage(models.Model):
    statusField = models.TextField(max_length=299)
    timeField = models.DateTimeField(auto_now_add=True)
