import unittest
# berfungsi sebagai memberikan waktu senggang agar hasil dari functional test dapat terlihat secara jelas
import time
from django.test import TestCase, selenium
from django.test import Client
from django.urls import resolve
from .views import landingPage
from .models import statusmessage
from selenium import webdriver
# memanggil special keys class yang di sediakan selenium
from selenium.webdriver.common.keys import Keys
# memanggil module Options yang dapat bergfungsi sebagai pemberi opsi
# untuk bagaimana chromedriver yang kita inginkan berjalan
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class Lab6UnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, landingPage)

    #
    def test_landingpage_containt(self):
        response = self.client.get('')
        html_response = response.content.decode('utf8')
        self.assertIn('Hello apa kabar?', html_response)

    # jika bisa tertambah 1 status maka test akan jalan
    def test_can_create_new_status(self):
        testText = 'Abiyu tampan'
        new_act = statusmessage.objects.create(statusField=testText)
        counting_all_available_status = statusmessage.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/', {'statusField': test})
        self.assertEqual(response_post.status_code, 302)  # 302 = Redirect

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    # def test_post_error_and_render_the_result(self):
    #     test = 'Anonymous'
    #     response_post = Client().post('/', {'title': '', 'description': ''})
    #     self.assertEqual(response_post.status_code, 302)

    #     response= Client().get('/lab-5/')
    #     html_response = response.content.decode('utf8')
    #     self.assertNotIn(test, html_response)

    def test_form_Status_rendered(self):
        # form = StatusForm()
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn('status', html_response)

    def test_button_submit(self):
        testText = 'Abiyu tampan'
        new_act = statusmessage.objects.create(statusField=testText)
        counting_all_available_status = statusmessage.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_TextField_300char(self):
        testtext = "a" * 301
        response = Client().post("/", {"statusField": testtext})
        # self.asserIsNotNone(stats)
        self.assertEqual(statusmessage.objects.filter(statusField=testtext).count(), 0)

    def test_url_is_not_Exsist(self):
        response = Client().get('kakpewe/')
        self.assertEqual(response.status_code, 404)

    #### CHALLANGE ####
    def test_check_PortofolioTEXT(self):
        response = self.client.get('/newPage/')
        html_response = response.content.decode('utf8')
        self.assertIn('EDUCATION', html_response)

    def test_check_JumbotronTEXT_name(self):
        response = self.client.get('/newPage/')
        html_response = response.content.decode('utf8')
        self.assertIn("I'm Abiyu Muhammad Akmal", html_response)

    def test_check_JumbotronTEXT_welcome(self):
        response = self.client.get('/newPage/')
        html_response = response.content.decode('utf8')
        self.assertIn("Welcome to my website profile page!", html_response)

    def test_check_FasilkomText(self):
        response = self.client.get('/newPage/')
        html_response = response.content.decode('utf8')
        self.assertIn("FASILKOM UNIVERSITAS INDONESIA", html_response)

    def test_check_abiyu_quoteText(self):
        response = self.client.get('/newPage/')
        html_response = response.content.decode('utf8')
        self.assertIn("NEVER GIVE UP IN YOUR DREAMS!", html_response)

    def test_check_SMAN1BOGOR(self):
        response = self.client.get('/newPage/')
        html_response = response.content.decode('utf8')
        self.assertIn("SMA NEGERI 1 BOGOR", html_response)

    def test_check_SMPN1BOGOR(self):
        response = self.client.get('/newPage/')
        html_response = response.content.decode('utf8')
        self.assertIn("SMP NEGERI 1 BOGOR", html_response)

    def test_check_foto_profile(self):
        response = self.client.get('/newPage/')
        html_response = response.content.decode('utf8')
        self.assertIn("https://image.ibb.co/bBoCCU/Foto_profile.png", html_response)

    def test_check_foto_FASILKOM(self):
        response = self.client.get('/newPage/')
        html_response = response.content.decode('utf8')
        self.assertIn("https://image.ibb.co/fbzWPz/fasilkom.png", html_response)

    def test_check_foto_SMA(self):
        response = self.client.get('/newPage/')
        html_response = response.content.decode('utf8')
        self.assertIn("https://image.ibb.co/cXgBPz/Smansa.png", html_response)

    def test_check_foto_SMP(self):
        response = Client().get('/newPage/')
        html_response = response.content.decode('utf8')
        self.assertIn("https://image.ibb.co/jWkhWe/Spensa.png", html_response)

    def test_url_is_exist_newPage(self):
        response = Client().get('/newPage/')
        self.assertEqual(response.status_code, 200)


class Story7FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver',
                                        chrome_options=chrome_options)
        super(Story7FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(Story7FunctionalTest, self).tearDown()

    def test_input_status_field(self):
        # Selenium adalah sebuah browser yang akan di tes melalui paradigma functional test
        selenium = self.browser

        # Opening the link we want to test
        selenium.get('http://localhost:8000')

        # find the form element
        statusfield_coloumn = selenium.find_element_by_id('id_statusField')
        submit = selenium.find_element_by_id('id_submit_button')

        # Fill the form with data
        statusfield_coloumn.send_keys('Test Fungsionalitas Status Massage')

        # submitting the form
        submit.send_keys(Keys.RETURN)
        time.sleep(5)  # buat nunggu 5 detik
        self.assertIn('Test Fungsionalitas Status Massage', self.browser.page_source)

    # Ngecek lay-out saat setelah submit
    def test_title_appear_when_submit(self):
        selenium = self.browser
        selenium.get('http://localhost:8000')
        statusfield_coloumn = selenium.find_element_by_id('id_statusField')
        submit = selenium.find_element_by_id('id_submit_button')
        statusfield_coloumn.send_keys('Test Fungsionalitas - Title tetap ada')
        submit.send_keys(Keys.RETURN)
        time.sleep(5)  # buat nunggu 5 detik
        self.assertIn('Hello apa kabar?', self.browser.page_source)

    def test_title_table_appear_when_submit(self):
        selenium = self.browser
        selenium.get('http://localhost:8000')
        statusfield_coloumn = selenium.find_element_by_id('id_statusField')
        submit = selenium.find_element_by_id('id_submit_button')
        statusfield_coloumn.send_keys('Test Fungsionalitas - Title tetap ada')
        submit.send_keys(Keys.RETURN)
        time.sleep(5)  # buat nunggu 5 detik
        self.assertIn('My Status Timeline', self.browser.page_source)

    def test_table_using_class_table_responsive_by_bootsrap(self):
        selenium = self.browser
        selenium.get('http://localhost:8000')
        statusfield_coloumn = selenium.find_element_by_id('id_statusField')
        submit = selenium.find_element_by_id('id_submit_button')
        statusfield_coloumn.send_keys('Test Fungsionalitas - TABLE pakai class butsrep')
        submit.send_keys(Keys.RETURN)
        time.sleep(5)  # buat nunggu 5 detik
        table_view_status = selenium.find_element_by_id('id_table_status_timeline')
        self.assertEquals('table-responsive', table_view_status.get_attribute('class'))

    def test_field_status_using_css_by_bootsrap(self):
        selenium = self.browser
        selenium.get('http://localhost:8000')
        background_body = selenium.find_element_by_tag_name('body')
        time.sleep(5)  # buat nunggu 5 detik
        self.assertEquals('background-color: whitesmoke;', background_body.get_attribute('style'))
