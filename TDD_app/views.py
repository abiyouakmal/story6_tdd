from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import statusForm
from .models import statusmessage

response = {}

def newPage(request):
    return render(request, 'newPage.html')

def landingPage(request):
    message = statusmessage.objects.all()
    if request.method == 'POST':
        form = statusForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')

    else:
        form = statusForm()
    return render(request, 'index.html', {'form': form, 'message': message})